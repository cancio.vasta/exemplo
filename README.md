# Estudo do Git

Um repositório de exemplo para realizar testes utilizando configurações avançadas do git em conjunto com o gitlab.

- [Issues](docs/issues.md)
- [Merge Requests](docs/merge_requests.md)